
import pandas as pd
import psycopg2

addDate="May16"

db_params = {
"host" : "jiovishwam-mhere-ci-postgres-prod-1-replica.postgres.database.azure.com",
"port" : 5432,
"dbname" :  "mhere",
"user" : "postgres@jiovishwam-mhere-ci-postgres-prod-1-replica",
"password" : "GuGP4Pe6GfxBpmXG",
"sslmode" : "require"
}

query="select xcall_id,ref_xcall_id,path,tag,dc_code,user_id,created_at,response_code,app_id,issue_curations->>'fl',issue_curations->>'fr',properties->>'sigmoidResult',properties->>'deviceOs',properties->>'sufi_threshold',properties->>'sufi_score',properties->>'distance',properties->>'distanceStr',properties->>'goldenmatch',properties->>'gs_match_count',properties->>'IsFaceOcclusion',properties->>'IsDarkLightBlur',properties->>'mask_occlusion_score',properties->>'face_occlusion_score', issue_curations->>'blur', issue_curations->>'fake', issue_curations->>'beard', issue_curations->>'visor', issue_curations->>'others', issue_curations->>'frontal', issue_curations->>'fullMask', issue_curations->>'lowLight', issue_curations->>'darkLight', issue_curations->>'eyesClose', issue_curations->>'nonFrontal', issue_curations->>'partialMask', issue_curations->>'capOcclusion', issue_curations->>'maskAndVisor', issue_curations->>'partialFaces', issue_curations->>'belowChinMask', issue_curations->>'gamchaOcclusion', issue_curations->>'helmetOcclusion', issue_curations->>'lowQualityImage', issue_curations->>'scarfsOcclusion', issue_curations->>'improperLighting',issue_curations->>'sunGlassesOcclusion', issue_curations->>'clearFrontalFace', issue_curations->>'partialFace', issue_curations->>'handsOnFace',issue_curations->>'regImageNotClear' from monitor_transactions where (tag = '2' OR tag= '4' OR tag = '6' OR tag = '8')  and path = '/v1/single_gesture' and (created_at :: DATE = CURRENT_DATE:: DATE-INTERVAL '13 day' );"

query_prod = "select xcall_id as xcall_id, app_id as app_id, user_id as user_id, response_code as response_code, properties->>'distance' as distance, properties->>'sufi_score' as sufi_score, properties->>'goldenMatch' as goldenMatch, properties->>'gs_match_count' as gs_match_count, properties->>'distanceStr' as distanceStr, properties->>'sufiGoldenMatch' as sufiGoldenMatch, properties->>'gs_sufi_match_count' as gs_sufi_match_count, properties->>'sufiScoreStr' as sufiScoreStr, properties->>'deviceOs' as deviceOs, properties->>'fiopOldMean' as fiopOldMean, properties->>'fiopIdMean' as fiopIdMean, properties->>'uniMean' as uniMean, properties->>'sigmoidResult_1' as sigmoidResult_1, properties->>'sigmoidResult_2' as sigmoidResult_2, properties->>'sigmoidResult_I' as sigmoidResult_I, properties->>'sigAndroidThreshold_1' as sigAndroidThreshold_1, properties->>'sigAndroidThreshold_2' as sigAndroidThreshold_2,properties->>'sigIphoneThreshold' as sigIphoneThreshold, properties->>'fiopAdam13Mean' as fiopAdam13Mean, properties->>'LiveAdam13_TH' as LiveAdam13_TH,properties->>'light_blur_scoref' as light_blur_scoref, properties->>'IOSfiopAdam18Mean' as IOSfiopAdam18Mean, properties->>'IOSLiveAdam18_TH' as IOSLiveAdam18_TH, properties->>'face_occlusion_score' as face_occlusion_score, properties->>'mask_occlusion_score' as mask_occlusion_score, properties->>'IsFaceOcclusion' as IsFaceOcclusion, properties->>'IsMaskOcclusion' as IsMaskOcclusion, properties->>'is_frm' as fr_mand_flag, properties->>'IsDC' as IsDC, properties->>'IsDCResponse' as IsDCResponse, path, created_at from transactions where (path = '/v1/single_gesture' or path = '/v1/me/reference') and (created_at :: DATE = CURRENT_DATE:: DATE-INTERVAL '13 day')"


columns_49 = ['xcall_id','ref_xcall_id','path','tag',
                  'dc_code','user_id','created_at','response_code','app_id','FL','FR','sigmoid_result',
                  'deviceOs','sufi_threshold','sufi_score','distance','distanceStr','goldenmatch',
                  'gs_match_count','IsFaceOcclusion','IsMaskOcclusion','mask_occlusion_score','face_occlusion_score',
                  'blur','fake','beard','visor','others','frontal','fullMask','lowLight','darkLight','eyesClose',
                  'nonFrontal','partialMask','capOcclusion','maskAndVisor','partialFaces','belowChinMask',
                  'gamchaOcclusion','helmetOcclusion','lowQualityImage','scarfsOcclusion','improperLighting',
                  'sunGlassesOcclusion','clearFrontalFace','partialFace','handsOnFace','regImageNotClear']

prod_columns=['xcall_id', 'app_id', 'user_id', 'response_code', 'distance',
       'sufi_score', 'goldenMatch', 'gs_match_count', 'distanceStr',
       'sufiGoldenMatch', 'gs_sufi_match_count', 'sufiScoreStr', 'deviceOs',
       'fiopOldMean', 'fiopIdMean', 'uniMean', 'sigmoidResult_1',
       'sigmoidResult_2', 'sigmoidResult_I', 'sigAndroidThreshold_1',
       'sigAndroidThreshold_2', 'sigIphoneThreshold', 'fiopAdam13Mean',
       'LiveAdam13_TH', 'light_blur_scoref', 'IOSfiopAdam18Mean',
       'IOSLiveAdam18_TH', 'face_occlusion_score', 'mask_occlusion_score',
       'IsFaceOcclusion', 'IsMaskOcclusion', 'fr_mand_flag', 'IsDC',
       'IsDCResponse', 'path', 'created_at']


