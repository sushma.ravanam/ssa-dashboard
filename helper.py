
import pandas as pd
import numpy as np
import os
from tqdm import tqdm

files=os.listdir('input/')
files = ['input/'+i for i in files]
files

result = pd.DataFrame()
def calc(df, date):
    df = df.loc[df.path != '/v1/me/reference']
    df = df[(df['tag']==2) | (df['tag']==4) | (df['tag']==6)|(df['tag']==8)]
    df200 = df.loc[(df.response_code == 200)]
    Total_200_response_code = len(df200)
    Total_FL_FP = len(df200.loc[(df200.dc_code.isin(['yes', 'quality_issue']))&((df200.FL == 'digital_fake')|
                                                                                    (df200.FL == 'paper_fake'))])


    Total_FR_FP = len(df200.loc[(df200.dc_code.isin(['yes', 'quality_issue']))&
                      (df200.FR == 'diffPerson')
                     ])

    Total_Occlusion_FP = len(df200.loc[(df200.dc_code.isin(['yes', 'quality_issue']))&
                      ((df200.fullMask == True)|(df200.partialMask == True)|(df200.maskAndVisor == True) |
                      (df200.capOcclusion == True)|(df200.gamchaOcclusion == True)|(df200.helmetOcclusion == True)|
                      (df200.scarfsOcclusion == True) |(df200.sunGlassesOcclusion == True)|(df200.handsOnFace == True))])
    Total_FP = Total_FL_FP + Total_FR_FP + Total_Occlusion_FP
    Total_TP = Total_200_response_code - Total_FP


    df424 = df.loc[(df.response_code == 424) ]
    Total_424 = len(df424)
    Total_424_FN = len(df424.loc[
                       (df424.fullMask == False)&(df424.partialMask == False)&(df424.maskAndVisor == False) &
                      (df424.capOcclusion == False)&(df424.gamchaOcclusion == False)&(df424.helmetOcclusion == False)&
                      (df424.scarfsOcclusion == False)&(df424.sunGlassesOcclusion == False)&(df424.handsOnFace == False)|
                     (df424.belowChinMask == True)|(df424.frontal == True)|(df424.clearFrontalFace == True)])

    Total_424_TN = Total_424 - Total_424_FN
    Total_FL_TP = Total_200_response_code - Total_FL_FP
    Total_FR_TP = Total_200_response_code - Total_FR_FP
    Total_Occlusion_TP = Total_200_response_code - Total_Occlusion_FP

    df404 = df.loc[(df.response_code == 404) ]
    Total_404 = len(df404)
    Total_404_FN =len(df404.loc[(df404.dc_code.isin(['yes', 'quality_issue']))&(df404.FR == 'samePerson')])
    Total_404_TN = Total_404 - Total_404_FN

    df406 = df.loc[(df.response_code == 406)]
    Total_406 = len(df406)
    Total_406_FN = len(df406.loc[(df406.dc_code.isin(['yes', 'quality_issue']))&((df406.FL == 'real')|(df406.FL=='bad_quality'))])
    Total_406_TN = Total_406 - Total_406_FN

    Total_FN = Total_424_FN + Total_404_FN + Total_406_FN
    Total_TN = Total_424_TN + Total_404_TN + Total_406_TN


    dict_ = {'FL_FP':Total_FL_FP, 'FR_FP':Total_FR_FP,'Occlusion_FP':Total_Occlusion_FP,'FL_TP':Total_FL_TP,'FR_TP':Total_FR_TP,
             'Occlusion_TP':Total_Occlusion_TP,'FL_FN':Total_406_FN,'FR_FN':Total_404_FN,'Occlusion_FN':Total_424_FN,'FL_TN':Total_406_TN, 
             'FR_TN':Total_404_TN,'Occlusion_TN':Total_424_TN}

    appid = date
    dd = pd.DataFrame.from_dict(dict_,orient='index',columns = [appid])
    global result
    result = pd.concat([result,dd], axis =1)



final_df=pd.DataFrame()
for i in tqdm(files):
    df = pd.read_csv(i)
    final_df=pd.concat([df,final_df])

calc(final_df,'data')

